package com.applaudostudios.newsapp.apiclient;

public class ApiClient {

    /**
     * This interface contains the base url and two urls to make the first request and to do the paging
     */
    private interface Api {
        String host = "https://content.guardianapis.com/";
        String URL_SECTION_NEWS = host + "search?section={section}&show-fields=thumbnail%2CbodyText%2Cheadline&page={numPage}&page-size={pageSize}&api-key=f2cf21b7-1c65-4249-9855-d52f0e37d8a5";
        String URL_SEARCH = host + "search?show-fields=headline%2CbodyText%2Cthumbnail&page={numPage}&page-size={pageSize}&q={query}&api-key=f2cf21b7-1c65-4249-9855-d52f0e37d8a5";
    }

    public static final String[] sections = {
            "politics",
            "sport",
            "technology",
            "culture",
            "education",
            "fashion",
            "world"
    };

    /**
     * This interface contains constants with the values ​​of the defined categories
     */
    public interface Categories {
        int CATEGORY_POLITICS = 1;
        int CATEGORY_WORLD = 2;
        int CATEGORY_SPORT = 3;
        int CATEGORY_TECHNOLOGY = 4;
        int CATEGORY_CULTURE = 5;
        int CATEGORY_EDUCATION = 6;
        int CATEGORY_FASHION = 7;
    }

    /**
     * This function takes a base url and replaces some values ​​such as section,
     * page to form the url to make a request
     *
     * @param category category to change to section
     * @param pageSize page size
     * @return a string with the url formed
     */
    public static String getUrlByCategory(int category, int pageSize) {
        switch (category) {
            case Categories.CATEGORY_POLITICS:
                return Api.URL_SECTION_NEWS.replace("{section}", sections[0])
                        .replace("{pageSize}", String.valueOf(pageSize));
            case Categories.CATEGORY_WORLD:
                return Api.URL_SECTION_NEWS.replace("{section}", sections[1])
                        .replace("{pageSize}", String.valueOf(pageSize));
            case Categories.CATEGORY_SPORT:
                return Api.URL_SECTION_NEWS.replace("{section}", sections[2])
                        .replace("{pageSize}", String.valueOf(pageSize));
            case Categories.CATEGORY_TECHNOLOGY:
                return Api.URL_SECTION_NEWS.replace("{section}", sections[3])
                        .replace("{pageSize}", String.valueOf(pageSize));
            case Categories.CATEGORY_CULTURE:
                return Api.URL_SECTION_NEWS.replace("{section}", sections[4])
                        .replace("{pageSize}", String.valueOf(pageSize));
            case Categories.CATEGORY_EDUCATION:
                return Api.URL_SECTION_NEWS.replace("{section}", sections[5])
                        .replace("{pageSize}", String.valueOf(pageSize));
            case Categories.CATEGORY_FASHION:
                return Api.URL_SECTION_NEWS.replace("{section}", sections[6])
                        .replace("{pageSize}", String.valueOf(pageSize));
            default:
                return Api.URL_SEARCH.replace("{pageSize}", String.valueOf(pageSize));
        }
    }

}
