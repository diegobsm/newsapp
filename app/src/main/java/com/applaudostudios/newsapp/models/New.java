package com.applaudostudios.newsapp.models;

import android.os.Parcel;
import android.os.Parcelable;

public class New implements Parcelable {

    private String mHeadLine;
    private String mBodyText;
    private String mThumbnail;
    private String mWebUrl;


    /**
     * Constructor that starts all model attributes
     *
     * @param headLine  start mHeadLine is of type String
     * @param bodyText  start mBodyText is of type String
     * @param thumbnail start mThumbnail is of type String
     * @param webUrl    start mWebUrl is of type String
     */
    public New(String headLine, String bodyText, String thumbnail, String webUrl) {
        mHeadLine = headLine;
        mBodyText = bodyText;
        mThumbnail = thumbnail;
        mWebUrl = webUrl;
    }

    public String getHeadLine() {
        return mHeadLine;
    }

    public void setHeadLine(String headLine) {
        mHeadLine = headLine;
    }

    public String getBodyText() {
        return mBodyText;
    }

    public void setBodyText(String bodyText) {
        mBodyText = bodyText;
    }

    public String getThumbnail() {
        return mThumbnail;
    }

    public void setThumbnail(String thumbnail) {
        mThumbnail = thumbnail;
    }

    public String getWebUrl() {
        return mWebUrl;
    }

    public void setWebUrl(String webUrl) {
        mWebUrl = webUrl;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mHeadLine);
        dest.writeString(this.mBodyText);
        dest.writeString(this.mThumbnail);
        dest.writeString(this.mWebUrl);
    }

    protected New(Parcel in) {
        this.mHeadLine = in.readString();
        this.mBodyText = in.readString();
        this.mThumbnail = in.readString();
        this.mWebUrl = in.readString();
    }

    public static final Parcelable.Creator<New> CREATOR = new Parcelable.Creator<New>() {
        @Override
        public New createFromParcel(Parcel source) {
            return new New(source);
        }

        @Override
        public New[] newArray(int size) {
            return new New[size];
        }
    };
}
