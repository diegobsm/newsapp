package com.applaudostudios.newsapp.activities;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.applaudostudios.newsapp.R;
import com.applaudostudios.newsapp.adapters.NewsViewPagerAdapter;
import com.applaudostudios.newsapp.apiclient.ApiClient;

public class MainActivity extends AppCompatActivity {

    private NewsViewPagerAdapter mNewsPagerAdapter;
    private ViewPager mVpgNew;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    /**
     * initialize the views.
     */
    public void initView() {
        mVpgNew = findViewById(R.id.vpgNews);
        mVpgNew.setOffscreenPageLimit(1);
        TabLayout tbl = findViewById(R.id.tblNews);
        mNewsPagerAdapter = new NewsViewPagerAdapter(getSupportFragmentManager());
        populateViewPager();
        mVpgNew.setAdapter(mNewsPagerAdapter);
        tbl.setupWithViewPager(mVpgNew);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mnActionSearch:
                startActivity(SearchActivity.newIntent(this));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * add 7 string with the 7 categories and their respective id
     */
    public void populateViewPager() {
        for (int i = 1; i <= 7; i++) {
            mNewsPagerAdapter.addTitleAndId(ApiClient.sections[i - 1], i);
        }
    }

}
