package com.applaudostudios.newsapp.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.applaudostudios.newsapp.R;
import com.applaudostudios.newsapp.fragments.DetailFragment;
import com.applaudostudios.newsapp.models.New;

public class DetailActivity extends AppCompatActivity {

    public static final String EXT_NEWS = "EXT_NEWS";

    /**
     * creates and returns an intent to DetailActivity with the data that is to be displayed in the fragment
     * @param context for create the intent
     * @param news data to load the detail of the news
     * @return intent
     */
    public static Intent newIntent(Context context, New news) {
        Intent intent = new Intent(context, DetailActivity.class);
        if (news != null) {
            intent.putExtra(EXT_NEWS, news);
        }
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        setBackArrow();
        loadFragment(getIntent().getExtras());

    }


    /**
     * set back arrow to actionBar
     */
    public void setBackArrow() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Load the fragment in the R.id.contFragment container for show detail
     *
     * @param bundle the data that is going to be sent to the fragment of detail
     */
    public void loadFragment(Bundle bundle) {
        if (bundle != null) {
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            DetailFragment detailFragment = new DetailFragment();
            detailFragment.setArguments(bundle);
            ft.add(R.id.contFragment, detailFragment).commit();
        }
    }

}
