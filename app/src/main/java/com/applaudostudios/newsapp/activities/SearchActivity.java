package com.applaudostudios.newsapp.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;

import com.applaudostudios.newsapp.R;
import com.applaudostudios.newsapp.fragments.ListNewsFragment;

public class SearchActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private static final String TAG_SEARCH = "TAG_SEARCH";
    private Handler mHandle;

    /**
     * creates and returns an intent to SearchActivity with the data that is to be displayed in the fragment
     *
     * @param context for create intent
     * @return intent
     */
    public static Intent newIntent(Context context) {
        return new Intent(context, SearchActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        setBackArrow();
        loadFragment(ListNewsFragment.newInstance(0, true));
        mHandle = new Handler();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);
        MenuItem search = menu.findItem(R.id.mnSearch);
        SearchView mSearchView = (SearchView) search.getActionView();
        mSearchView.setQueryHint(getString(R.string.placeholder_searchview));
        mSearchView.setOnQueryTextListener(this);
        mSearchView.setIconified(false);
        mSearchView.requestFocusFromTouch();
        return true;
    }

    /**
     * set back arrow to actionBar
     */
    public void setBackArrow() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Load the fragment in the R.id.ctlSearch container
     *
     * @param fragment instance of the ListNewsFragment
     */
    public void loadFragment(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.ctlSearch, fragment, TAG_SEARCH).commit();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        ListNewsFragment listNewsFragment = (ListNewsFragment) getSupportFragmentManager().findFragmentByTag(TAG_SEARCH);
        if (listNewsFragment != null) {
            if (!query.equals("")) {
                listNewsFragment.filterData(query);
            } else {
                listNewsFragment.cleanData();
            }
        }
        return false;
    }

    @Override
    public boolean onQueryTextChange(final String newText) {
        final ListNewsFragment listNewsFragment = (ListNewsFragment) getSupportFragmentManager().findFragmentByTag(TAG_SEARCH);
        if (listNewsFragment != null) {
            if (!newText.equals("")) {
                mHandle.removeCallbacksAndMessages(null);
                mHandle.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        listNewsFragment.filterData(newText);
                    }
                }, 1000);

            } else {
                mHandle.removeCallbacksAndMessages(null);
                listNewsFragment.cleanData();
            }
        }
        return false;
    }

}
