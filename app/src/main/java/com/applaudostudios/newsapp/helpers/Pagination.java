package com.applaudostudios.newsapp.helpers;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.applaudostudios.newsapp.loaders.RequestLoader;
import com.applaudostudios.newsapp.models.New;

import java.util.List;

public class Pagination extends RecyclerView.OnScrollListener implements LoaderManager.LoaderCallbacks<String> {

    private static final String EXT_URL = "EXT_URL";
    private OnDataAvailable mListener;
    private int mPageSize;
    private int mNumberPager;
    private boolean isLoading;
    private int mCategory;
    private FragmentActivity mActivity;


    private String mUrl;

    public Pagination(OnDataAvailable dataAvailable, FragmentActivity activity,
                      int numberPage, int pageSize, int category, String url) {
        mListener = dataAvailable;
        mNumberPager = numberPage;
        mPageSize = pageSize;
        mActivity = activity;
        mCategory = category;
        mUrl = url;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        LinearLayoutManager lym = (LinearLayoutManager) recyclerView.getLayoutManager();
        int visibleItemCount = lym.getChildCount();
        int totalItemCount = lym.getItemCount();
        int firstVisibleItemPosition = lym.findFirstVisibleItemPosition();

        if (!isLoading) {
            if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                    && firstVisibleItemPosition >= 0
                    && totalItemCount >= mPageSize) {
                mListener.startRequest();
                loadMoreItems();
            }
        }
    }

    public void setmUrl(String mUrl) {
        this.mUrl = mUrl;
    }

    @Override
    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);
    }

    /**
     * starts if the Loader that makes the request to download new data
     * to the api if it is not created or restarts it
     */
    private void loadMoreItems() {
        Bundle bundle = new Bundle();
        String url = mUrl;
        url = url.replace("{numPage}", String.valueOf(mNumberPager));
        bundle.putString(EXT_URL, url);
        mNumberPager++;
        mActivity.getSupportLoaderManager().restartLoader(mCategory, bundle, this);
        isLoading = true;
    }

    @NonNull
    @Override
    public Loader<String> onCreateLoader(int id, @Nullable Bundle args) {
        if (args != null) {
            String url = args.getString(EXT_URL);
            return new RequestLoader(mActivity, url);
        }
        return null;
    }

    @Override
    public void onLoadFinished(@NonNull Loader<String> loader, String data) {
        List<New> news = ParseJsonNews.getNews(data);
        if (!news.isEmpty()) {
            mListener.dataAvailable(news);
        }
        isLoading = false;
    }

    @Override
    public void onLoaderReset(@NonNull Loader<String> loader) {

    }


    /**
     * Define two callbacks, to notify when a new request is being made and that the fragment shows a progress,
     * and the other when the data is available and the fragment updates the data in the recyclerView
     */
    public interface OnDataAvailable {
        /**
         * This function haves a parameter of type List<New> with the data new
         * from the API
         *
         * @param news Type List<String>
         */
        void dataAvailable(List<New> news);

        /**
         * This function notifies when you start a new request
         */
        void startRequest();
    }
}
