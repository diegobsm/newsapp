package com.applaudostudios.newsapp.helpers;

import com.applaudostudios.newsapp.models.New;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ParseJsonNews {

    /**
     * This function takes a json as a parameter
     * and using JsonObject and JsonArray accesses the json
     * and obtains the necessary information to create the model
     *
     * @param json used to obtain the data to create the model
     * @return a list of type New
     */
    public static List<New> getNews(String json) {
        if (json != null) {
            List<New> news = new ArrayList<>();
            try {
                JSONObject newsObject = new JSONObject(json);
                JSONObject responseObject = newsObject.getJSONObject("response");
                JSONArray resultArray = responseObject.getJSONArray("results");
                for (int i = 0; i < resultArray.length(); i++) {
                    JSONObject result = resultArray.getJSONObject(i);
                    JSONObject field = result.getJSONObject("fields");

                    New noti = new New(
                            field.has("headline") ? field.getString("headline") : "",
                            field.has("bodyText") ? field.getString("bodyText") : "",
                            field.has("thumbnail") ? field.getString("thumbnail") : "",
                            result.has("webUrl") ? result.getString("webUrl") : "");
                    news.add(noti);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return news;
        }
        return null;
    }
}
