package com.applaudostudios.newsapp.helpers;

public class UrlBuilder {

    public static final String SECTION_POLITICS = "politics";
    public static final String SECTION_SPORT = "sport";
    public static final String SECTION_TECHNOLOGY = "technology";
    public static final String SECTION_CULTURE = "culture";
    public static final String SECTION_EDUCATION = "education";
    public static final String SECTION_FASHION = "fashion";
    public static final String SECTION_WORLD = "world";

    public static class Build {
        private static StringBuilder url;
        private static String mSection;
        private static String[] mFields;
        private static String defaultField = "all";
        private static String mApiKey;
        private static int mPageSize = 10;
        private static int mCurrentPage = 1;

        public Build(String domain, String apiKey) {
            url = new StringBuilder();
            url.append(domain);
            mApiKey = apiKey;
            mSection = "";
        }


        public static void setSection(String section) {
            mSection = section;
        }

        public static void setmFields(String... fields) {
            mFields = fields;
        }

        public static void setPageSize(int pageSize) {
            mPageSize = pageSize;
        }

        public static void setPage(int page) {
            mCurrentPage = page;
        }

        public static String build() {
            url.append("/search?");
            if (!mSection.equals("")) {
                url.append("section=").append(mSection);
            }

            if (mFields != null && mFields.length > 0) {
                url.append("&show-fields=");
                for (int i = 0; i < mFields.length; i++) {
                    if (mFields.length == (i + 1)) {
                        url.append(mFields[i]);
                    } else {
                        url.append(mFields[i]).append("%2C");
                    }
                }
            } else {
                url.append("&show-fields=all");
            }

            return url.toString();
        }
    }

}

