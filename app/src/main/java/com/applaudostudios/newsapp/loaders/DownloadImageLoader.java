package com.applaudostudios.newsapp.loaders;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.AsyncTaskLoader;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class DownloadImageLoader extends AsyncTaskLoader<Bitmap> {

    private String mUrlImage;

    public DownloadImageLoader(@NonNull Context context, String urlImage) {
        super(context);
        mUrlImage = urlImage;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }

    @Nullable
    @Override
    public Bitmap loadInBackground() {
        InputStream is = null;
        try {
            URL url = new URL(mUrlImage);
            is = url.openStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (is != null) {
            Bitmap bm = BitmapFactory.decodeStream(is);
            if (bm != null) {
                return bm;
            }
        }
        return null;
    }
}
