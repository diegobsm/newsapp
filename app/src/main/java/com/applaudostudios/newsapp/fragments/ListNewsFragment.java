package com.applaudostudios.newsapp.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.applaudostudios.newsapp.R;
import com.applaudostudios.newsapp.activities.DetailActivity;
import com.applaudostudios.newsapp.adapters.NewsAdapter;
import com.applaudostudios.newsapp.apiclient.ApiClient;
import com.applaudostudios.newsapp.helpers.Pagination;
import com.applaudostudios.newsapp.helpers.ParseJsonNews;
import com.applaudostudios.newsapp.loaders.RequestLoader;
import com.applaudostudios.newsapp.models.New;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListNewsFragment extends Fragment implements LoaderManager.LoaderCallbacks<String>,
        NewsAdapter.OnItemNewsClick, Pagination.OnDataAvailable {

    private static String EXT_ID_CATEGORY = "EXT_ID_CATEGORY";
    private static String EXT_URL = "EXT_URL";
    private static String EXT_SEARCH = "EXT_SEARCH";
    private ProgressBar mPgNews;
    Pagination mPagination;
    private int mCategory;
    private View mView;
    private NewsAdapter mAdapter;
    private List<New> mNews;


    public static ListNewsFragment newInstance(int category) {
        return newInstance(category, false);
    }

    /**
     * Return a instance of type ListNewsFragment
     *
     * @param category to get the url of that category
     * @param search   parameter so that it serves to identify if the fragment is loaded from the MainAcivity or SearchActivity
     * @return instance the ListNewsFragment
     */
    public static ListNewsFragment newInstance(int category, boolean search) {
        Bundle bundle = new Bundle();
        bundle.putInt(EXT_ID_CATEGORY, category);
        bundle.putBoolean(EXT_SEARCH, search);
        ListNewsFragment listNewsFragment = new ListNewsFragment();
        listNewsFragment.setArguments(bundle);
        return listNewsFragment;
    }

    public ListNewsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() != null) {
            if (getArguments() != null) {
                mCategory = getArguments().getInt(EXT_ID_CATEGORY);
                mAdapter = new NewsAdapter(this);
            }
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_list_news, container, false);
        return mView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments() != null) {
            mPgNews = mView.findViewById(R.id.pgNews);
            RecyclerView rcvNes = mView.findViewById(R.id.rcvNews);
            LinearLayoutManager lym = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
            rcvNes.setLayoutManager(lym);
            if (!getArguments().getBoolean(EXT_SEARCH)) {
                String urlBase = ApiClient.getUrlByCategory(getArguments().getInt(EXT_ID_CATEGORY), 10);
                mPagination = new Pagination(this, getActivity(), 4, 30, mCategory, urlBase);
                if (getActivity() != null) {
                    String urlRequest = ApiClient.getUrlByCategory(getArguments().getInt(EXT_ID_CATEGORY), 30);
                    urlRequest = urlRequest.replace("{numPage}",String.valueOf(1));
                    Bundle urlBundle = new Bundle();
                    urlBundle.putString(EXT_URL, urlRequest);
                    getActivity().getSupportLoaderManager().restartLoader(mCategory, urlBundle, this);
                    mPgNews.setVisibility(View.VISIBLE);
                }
            } else {
                mPagination = new Pagination(this, getActivity(), 2, 5, mCategory, "");
            }
            rcvNes.addOnScrollListener(mPagination);
            rcvNes.setAdapter(mAdapter);
        }
    }

    /**
     * Start the loader that makes the request when a letter is written or click on the search button in searchView
     *
     * @param query contains the string that the user wants to search
     */
    public void filterData(String query) {
        if (getActivity() != null) {
            mPgNews.setVisibility(View.VISIBLE);
            Bundle urlBundle = new Bundle();
            String url = ApiClient.getUrlByCategory(mCategory, 5);
            url = url.replace("{query}", query);
            mPagination.setmUrl(url);
            String urlRequest = url.replace("{numPage}",String.valueOf(1));
            urlBundle.putString(EXT_URL, urlRequest);
            getActivity().getSupportLoaderManager().restartLoader(mCategory, urlBundle, this);
        }
    }

    /**
     * call adapter cleanList() to clean restart the base data list
     */
    public void cleanData() {
        mAdapter.cleanList();
    }


    @NonNull
    @Override
    public Loader<String> onCreateLoader(int id, @Nullable Bundle args) {
        if (getContext() != null) {
            String url = "";
            if (args != null) {
                url = args.getString(EXT_URL);
            }
            return new RequestLoader(getContext(), url);
        }
        return null;
    }

    @Override
    public void onLoadFinished(@NonNull Loader<String> loader, String data) {
        mPgNews.setVisibility(View.GONE);
        if (data != null) {
            mNews = ParseJsonNews.getNews(data);
            if (mNews != null && mNews.size() > 0) {
                if (mAdapter != null) {
                    mAdapter.setmData(mNews);
                }
            }
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<String> loader) {

    }

    /**
     * Callback that executes when you click on an item in the RecyclerView,
     * and that makes an attempt to detail the news
     *
     * @param position parameter with the position of the selected item
     */
    @Override
    public void onNewClick(int position) {
        if (getContext() != null) {
            startActivity(DetailActivity.newIntent(getContext(), mNews.get(position)));
        }
    }

    /**
     * This is the callback that is executed when the request
     * for new data activated by the page has finished
     *
     * @param news It has a parameter of type List with the new page of datps
     */
    @Override
    public void dataAvailable(List<New> news) {
        mNews.addAll(news);
        mAdapter.setmData(mNews);
    }

    /**
     * notifies the adapter when a new data request has been initiated on the page
     */
    @Override
    public void startRequest() {
        mAdapter.setmData(mNews, true);
    }
}
