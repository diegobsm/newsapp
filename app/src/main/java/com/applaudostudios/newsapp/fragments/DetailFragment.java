package com.applaudostudios.newsapp.fragments;


import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.applaudostudios.newsapp.R;
import com.applaudostudios.newsapp.activities.DetailActivity;
import com.applaudostudios.newsapp.loaders.DownloadImageLoader;
import com.applaudostudios.newsapp.models.New;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailFragment extends Fragment implements LoaderManager.LoaderCallbacks<Bitmap>, View.OnClickListener {

    private final String EXT_IMG = "EXT_IMG";

    private View mView;
    private ImageView mImvThumbnail;
    private New mNew;

    public DetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() != null) {
            if (getActivity().getSupportLoaderManager().getLoader(0) != null) {
                getActivity().getSupportLoaderManager().initLoader(0, null, this);
            }
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.fragment_detail, container, false);
        return mView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getActivity() != null) {
            if (getArguments() != null) {
                Bundle bundle = getArguments();
                mNew = bundle.getParcelable(DetailActivity.EXT_NEWS);
                if (mNew != null) {
                    mImvThumbnail = mView.findViewById(R.id.imvDThumbnail);
                    TextView txvHeadline = mView.findViewById(R.id.txvDHeadline);
                    TextView txvBodyText = mView.findViewById(R.id.txvDBodyText);
                    Button btnWebUrl = mView.findViewById(R.id.btnDWebUrl);
                    btnWebUrl.setOnClickListener(this);
                    if (!mNew.getThumbnail().equals("")) {
                        Bundle urlBundle = new Bundle();
                        urlBundle.putString(EXT_IMG, mNew.getThumbnail());
                        getActivity().getSupportLoaderManager().restartLoader(0, urlBundle, this);
                    } else {
                        mImvThumbnail.setVisibility(View.GONE);
                    }
                    txvHeadline.setText(mNew.getHeadLine());
                    txvBodyText.setText(mNew.getBodyText());
                    if (mNew.getWebUrl().equals("")) {
                        btnWebUrl.setVisibility(View.VISIBLE);
                    }
                }
            }
        }

    }

    @NonNull
    @Override
    public Loader<Bitmap> onCreateLoader(int id, @Nullable Bundle args) {
        if (getContext() != null) {
            String urlImage = "";
            if (args != null) {
                urlImage = args.getString(EXT_IMG);
            }
            return new DownloadImageLoader(getContext(), urlImage);
        }
        return null;
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Bitmap> loader, Bitmap data) {
        if (data != null) {
            mImvThumbnail.setImageBitmap(data);
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Bitmap> loader) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnDWebUrl:
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(mNew.getWebUrl()));
                startActivity(intent);
        }
    }
}
