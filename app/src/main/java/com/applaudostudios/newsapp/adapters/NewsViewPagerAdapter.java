package com.applaudostudios.newsapp.adapters;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.applaudostudios.newsapp.fragments.ListNewsFragment;

import java.util.ArrayList;
import java.util.List;

public class NewsViewPagerAdapter extends FragmentStatePagerAdapter {

    private final List<String> mTitles;
    private final List<Integer> mCategoryId;

    public NewsViewPagerAdapter(FragmentManager fm) {
        super(fm);
        mTitles = new ArrayList<>();
        mCategoryId = new ArrayList<>();
    }

    /**
     * Insert a record of the Titles and Categories list for each category you have
     *
     * @param tabName    Name of the tab
     * @param idCategory category id
     */
    public void addTitleAndId(String tabName, int idCategory) {
        mTitles.add(tabName);
        mCategoryId.add(idCategory);
    }

    @Override
    public Fragment getItem(int position) {
        return ListNewsFragment.newInstance(mCategoryId.get(position));
    }

    @Override
    public int getCount() {
        return mCategoryId.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mTitles.get(position);
    }
}
