package com.applaudostudios.newsapp.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.applaudostudios.newsapp.R;
import com.applaudostudios.newsapp.models.New;

import java.util.ArrayList;
import java.util.List;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsViewHolder> {

    private OnItemNewsClick mListener;

    private List<New> mData;
    private int mCountLoad = 0;
    private boolean mIsLoad = false;

    public NewsAdapter(OnItemNewsClick onItemNewsClick) {
        mListener = onItemNewsClick;
        mData = new ArrayList<>();
    }

    public void setmData(List<New> data) {
        setmData(data, false);
    }

    /**
     * insert the initial data or notify the adapter to insert an extra record into the list
     * to show the progress in the last item because a request is being made to bring the new data page
     *
     * @param data     list with base data
     * @param itemLoad boolean that notify if add new row to list o only update the data
     */
    public void setmData(List<New> data, boolean itemLoad) {
        mData = data;
        mCountLoad = itemLoad ? 1 : 0;
        mIsLoad = itemLoad;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if (mIsLoad) {
            if (mData.size() == position) {
                return 1;
            } else {
                return super.getItemViewType(position);
            }
        } else {
            return super.getItemViewType(position);
        }
    }

    @NonNull
    @Override
    public NewsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if (viewType == 0) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_news, parent, false);
            return new NewsViewHolder(view);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_progress, parent, false);
            return new NewsViewHolder(view, viewType);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull NewsViewHolder holder, int position) {
        if (mIsLoad) {
            if (!((mData.size()) == position)) {
                holder.bind(mData.get(position));
            }
        } else {
            holder.bind(mData.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return mData.size() + mCountLoad;
    }

    /**
     * initialize the list that is taken as base data
     */
    public void cleanList() {
        mData = new ArrayList<>();
        notifyDataSetChanged();
    }

    /**
     * This viewHolder has two constructors one is used
     * when the view to be created is by the news list and the other is
     * when the item to be created is with the progress
     */
    public class NewsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView mTxvHeadline;
        private TextView mTxvBodyText;
        private CardView mCtlMain;

        NewsViewHolder(View itemView) {
            super(itemView);
            mTxvHeadline = itemView.findViewById(R.id.txvHeadline);
            mTxvBodyText = itemView.findViewById(R.id.txvBodyText);
            mCtlMain = itemView.findViewById(R.id.ctlMain);
            mCtlMain.setOnClickListener(this);
        }

        NewsViewHolder(View view, int type) {
            super(view);
        }

        private void bind(New news) {
            mTxvHeadline.setText((!news.getHeadLine().equals("")) ? news.getHeadLine() : "");
            mTxvBodyText.setText((!news.getBodyText().equals("")) ? news.getBodyText().substring(0, 150) : "");
        }

        @Override
        public void onClick(View v) {
            mListener.onNewClick(getAdapterPosition());
        }

    }


    /**
     * This interface is used as a callback between the adapter
     * and fragment when you click on an item in the list
     */
    public interface OnItemNewsClick {
        /**
         * callback that return the index with the position of the item that was clicked
         *
         * @param position index of the item
         */
        void onNewClick(int position);
    }
}
